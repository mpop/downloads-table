# install dependencies before running the app or the tests

use `npm install`

# how to run the app

use `npm start`

# how to run the tests

use `npm test` . I only wrote tests for `components/Downloads` because of the 4 hours time constraint