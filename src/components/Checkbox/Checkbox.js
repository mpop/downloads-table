import React from 'react';
import PropTypes from 'prop-types';

import './Checkbox.css';

function Checkbox({ id, label, isChecked, isIndeterminate, onChange }) {
  function onInputChange(event) {
    event.stopPropagation();

    onChange(id);
  }

  function onRef(element) {
    if (!element) {
      return;
    }

    element.indeterminate = isIndeterminate;
  }

  return (
    <div className="checkbox">
      <input
        className="checkbox__input"
        ref={onRef}
        id={id}
        name={id}
        type="checkbox"
        checked={isChecked}
        onChange={onInputChange}
      />
      <label className="checkbox__label" htmlFor={id}>{label}</label>
    </div>
  );
}

Checkbox.propTypes = {
  id: PropTypes.any.isRequired,
  label: PropTypes.string,
  isChecked: PropTypes.bool,
  isIndeterminate: PropTypes.bool,
  onChange: PropTypes.func.isRequired
};

Checkbox.defaultProps = {
  label: '',
  isChecked: false,
  isIndeterminate: false,
};

export default Checkbox;
