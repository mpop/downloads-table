import React from 'react';
import PropTypes from 'prop-types';

import './Sempahore.css';

function Semaphore({ isAllowed }) {
  const isAllowedClassName = isAllowed ? 'semaphore--is-allowed' : '';

  return <div className={`semaphore ${isAllowedClassName}`}></div>;
}

Semaphore.propTypes = {
  isAllowed: PropTypes.bool,
};

Semaphore.defaultProps = {
  isAllowed: false,
};

export default Semaphore;
