import React from 'react';
import PropTypes from 'prop-types';

import './Button.css';

function Button({ icon, children, onClick }) {
  let Icon = icon;

  if (Icon) {
    return (
      <button className="button button--icon" type="button" onClick={onClick}>
        <Icon className="button__icon" />
        {children}
      </button>
    );
  }

  return <button className="button" type="button" onClick={onClick}>{children}</button>
}

Button.propTypes = {
  icon: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.elementType,
    PropTypes.node,
  ]),
  children: PropTypes.any.isRequired,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  onClick: () => {},
};

export default Button;
