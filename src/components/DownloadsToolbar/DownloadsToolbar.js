import React from 'react';
import PropTypes from 'prop-types';
import Checkbox from 'components/Checkbox';
import Button from 'components/Button';
import { ReactComponent as DownloadIcon } from 'components/Icons/DownloadIcon.svg';
import isDownloadAvailable from 'utils/isDownloadAvailable';

import './DownloadsToolbar.css';

function DownloadsToolbar({ items, onSelectAll, onDeselectAll, onDownload }) {
  const availableItems = items.filter(({ status }) => isDownloadAvailable(status));
  const selectedItems = availableItems.filter(({ isSelected }) => isSelected);
  const selectedItemsLength = selectedItems.length;
  const isCheckboxChecked = selectedItemsLength === availableItems.length;
  const isCheckboxIndeterminate = selectedItemsLength > 0 && !isCheckboxChecked;
  const selectedItemsText = selectedItemsLength > 0 ? `Selected ${selectedItemsLength}` : 'None Selected';
  const isDownloadButtonVisible = selectedItemsLength > 0;

  function onCheckboxChange() {
    if (isCheckboxChecked) {
      onDeselectAll();
    } else {
      onSelectAll();
    }
  }

  function onDownloadButtonClick() {
    onDownload(selectedItems);
  }

  return (
    <div className="downloads-toolbar__container">
      <Checkbox
        id="toolbar"
        label={selectedItemsText}
        isChecked={isCheckboxChecked}
        isIndeterminate={isCheckboxIndeterminate}
        onChange={onCheckboxChange}
      />
      {isDownloadButtonVisible && (
        <Button icon={DownloadIcon} onClick={onDownloadButtonClick}>Download Selected</Button>
      )}
    </div>
  );
}

DownloadsToolbar.propTypes = {
  items: PropTypes.array.isRequired,
  onSelectAll: PropTypes.func.isRequired,
  onDeselectAll: PropTypes.func.isRequired,
  onDownload: PropTypes.func.isRequired,
};

export default DownloadsToolbar;
