import React from 'react';
import { mount } from 'enzyme';
import { act } from 'react-dom/test-utils';

import Downloads from './Downloads';

jest.mock('components/DownloadsTable', () => {
  const DownloadsTable = () => null;
  return DownloadsTable;
});

jest.mock('components/DownloadsToolbar', () => {
  const DownloadsToolbar = () => null;
  return DownloadsToolbar;
});

const itemsMock = [
  {name: 'smss.exe', device: 'Stark', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe', status: 'scheduled', isSelected: false},
  {name: 'netsh.exe', device: 'Targaryen', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\netsh.exe', status: 'available', isSelected: false},
  {name: 'uxtheme.dll', device: 'Lanniester', path: '\\Device\\HarddiskVolume1\\Windows\\System32\\uxtheme.dll', status: 'available', isSelected: false},
  {name: 'cryptbase.dll', device: 'Martell', path: '\\Device\\HarddiskVolume1\\Windows\\System32\\cryptbase.dll', status: 'scheduled', isSelected: false},
  {name: '7za.exe', device: 'Baratheon', path: '\\Device\\HarddiskVolume1\\temp\\7za.exe', status: 'scheduled'}
];

const hasSelectedItemsMock = [
  {name: 'smss.exe', device: 'Stark', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe', status: 'scheduled'},
  {name: 'netsh.exe', device: 'Targaryen', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\netsh.exe', status: 'available', isSelected: true},
  {name: 'uxtheme.dll', device: 'Lanniester', path: '\\Device\\HarddiskVolume1\\Windows\\System32\\uxtheme.dll', status: 'available', isSelected: true},
  {name: 'cryptbase.dll', device: 'Martell', path: '\\Device\\HarddiskVolume1\\Windows\\System32\\cryptbase.dll', status: 'scheduled'},
  {name: '7za.exe', device: 'Baratheon', path: '\\Device\\HarddiskVolume1\\temp\\7za.exe', status: 'scheduled'}
];

describe('Downloads component', () => {
  function setup() {
    return mount(<Downloads items={itemsMock} />);
  }

  it('renders', () => {
    expect(setup()).toMatchSnapshot();
  });

  it('should pass the items to DownloadsToolbar and DownloadsTable', () => {
    const wrapper = setup();
    const toolbar = wrapper.find('DownloadsToolbar');
    const table = wrapper.find('DownloadsTable');

    expect(toolbar.props().items).toEqual(itemsMock);
    expect(table.props().items).toEqual(itemsMock);
  });

  it('should mark all items as selected when DownloadsToolbar.onSelectAllItems is invoked', () => {
    const wrapper = setup();
    let toolbar = wrapper.find('DownloadsToolbar');

    act(() => {
      toolbar.props().onSelectAll();
    });

    wrapper.update();
    toolbar = wrapper.find('DownloadsToolbar');
    expect(toolbar.props().items.filter(item => item.isSelected)).toHaveLength(2);
  });

  it('should mark all items as deselected when DownloadsToolbar.onDeselectAllItems is invoked', () => {
    const wrapper = setup({
      items: hasSelectedItemsMock,
    });
    let toolbar = wrapper.find('DownloadsToolbar');

    act(() => {
      toolbar.props().onDeselectAll();
    });

    wrapper.update();
    toolbar = wrapper.find('DownloadsToolbar');
    expect(toolbar.props().items.filter(item => item.isSelected)).toHaveLength(0);
  });

  it('should not select an item if status is scheduled', () => {
    const wrapper = setup({
      items: [
        {name: 'smss.exe', device: 'Stark', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe', status: 'scheduled', isSelected: true},
      ]
    });
    let table = wrapper.find('DownloadsTable');

    act(() => {
      table.props().onToggleSelection({ name: 'smss.exe', status: 'scheduled' });
    });

    wrapper.update();
    table = wrapper.find('DownloadsTable');
    expect(table.props().items[0].isSelected).toBeFalsy();
  });

  it('should select an item if status is available', () => {
    const wrapper = setup();
    let table = wrapper.find('DownloadsTable');

    act(() => {
      table.props().onToggleSelection({ name: 'netsh.exe', status: 'available' });
    });

    wrapper.update();
    table = wrapper.find('DownloadsTable');
    expect(table.props().items[1].isSelected).toBeTruthy();
  });

  it('should alert the selected downloads when invoking DownloadsToolbar.onDownload', () => {
    jest.spyOn(window, 'alert').mockImplementation(() => {});

    const wrapper = setup({
      items: hasSelectedItemsMock
    });
    const toolbar = wrapper.find('DownloadsToolbar');
    toolbar.props().onDownload([
      {name: 'netsh.exe', device: 'Targaryen', path: '\\Device\\HarddiskVolume2\\Windows\\System32\\netsh.exe', status: 'available', isSelected: true},
      {name: 'uxtheme.dll', device: 'Lanniester', path: '\\Device\\HarddiskVolume1\\Windows\\System32\\uxtheme.dll', status: 'available', isSelected: true},
    ]);

    expect(window.alert).toHaveBeenCalledWith(`Targaryen - \\Device\\HarddiskVolume2\\Windows\\System32\\netsh.exe
Lanniester - \\Device\\HarddiskVolume1\\Windows\\System32\\uxtheme.dll
`);
  });
});
