import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import DownloadsTable from 'components/DownloadsTable';
import DownloadsToolbar from 'components/DownloadsToolbar';

import './Downloads.css';
import isDownloadAvailable from 'utils/isDownloadAvailable';

function Downloads({ items }) {
  const [internalItems, setInternalItems] = useState(undefined);

  useEffect(() => {
    setInternalItems(items);
  }, [items]);

  function onToggleItemSelection(item) {
    if(!isDownloadAvailable(item.status)) {
      return;
    }

    setInternalItems(prevItems => {
      return prevItems.map(prevItem => {
        if (prevItem.name === item.name) {
          return {
            ...prevItem,
            isSelected: !prevItem.isSelected,
          };
        }

        return prevItem;
      });
    })
  }

  function onSelectAllItems() {
    setInternalItems(prevItems => {
      return prevItems.map(prevItem => {
        if (!isDownloadAvailable(prevItem.status)) {
          return prevItem;
        }

        return {
          ...prevItem,
          isSelected: true
        };
      });
    });
  }

  function onDeselectAllItems() {
    setInternalItems(prevItems => {
      return prevItems.map(prevItem => {
        if (!isDownloadAvailable(prevItem.status)) {
          return prevItem;
        }

        return {
          ...prevItem,
          isSelected: false
        };
      });
    });
  }

  function onDownloadItems(selectedItems = []) {
    alert(selectedItems.reduce((accumulatedText, { device, path }) => `${accumulatedText}${device} - ${path}\n`, ''));
  }

  if (!internalItems || !internalItems.length) {
    return 'No downloads available';
  }

  return (
    <div className="downloads__container">
      <DownloadsToolbar
        items={internalItems}
        onSelectAll={onSelectAllItems}
        onDeselectAll={onDeselectAllItems}
        onDownload={onDownloadItems}
      />

      <DownloadsTable
        items={internalItems}
        onToggleSelection={onToggleItemSelection}
      />
    </div>
  );
}

Downloads.propTypes = {
  items: PropTypes.array.isRequired
};

export default Downloads;
