import React from 'react';
import PropTypes from 'prop-types';

import Checkbox from 'components/Checkbox';
import Semaphore from 'components/Semaphore';
import isDownloadAvailable from 'utils/isDownloadAvailable';

function DownloadsTableRow({ item, onToggleSelection }) {
  const selectedClassName = item.isSelected ? 'downloads-table__row--selected' : '';

  function onRowClick(event) {
    if (event.target.type === 'checkbox') {
      return;
    }

    onToggleSelection(item);
  }

  function onCheckoxChange() {
    onToggleSelection(item);
  }

  return (
    <tr className={`downloads-table__row ${selectedClassName}`} onClick={onRowClick}>
      <td className="downloads-table__content-cell downloads-table__cell--checkbox">
        <Checkbox
          id={item.name}
          isChecked={item.isSelected}
          onChange={onCheckoxChange}
        />
      </td>
      <td className="downloads-table__content-cell">{item.name}</td>
      <td className="downloads-table__content-cell">{item.device}</td>
      <td className="downloads-table__content-cell">{item.path}</td>
      <td className="downloads-table__content-cell downloads-table__cell--status">
        <div className="downloads-table__cell-status-content">
          <Semaphore isAllowed={isDownloadAvailable(item.status)} />
          {item.status}
        </div>
      </td>
    </tr>
  );
}

DownloadsTableRow.propTypes = {
  item: PropTypes.object.isRequired,
};

export default DownloadsTableRow;
