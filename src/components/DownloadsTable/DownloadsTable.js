import React from 'react';
import PropTypes from 'prop-types';

import DownloadsTableRow from './DownloadsTableRow';

import './DownloadsTable.css';

function DownloadsTable({ items, onToggleSelection }) {
  return (
    <table className="downloads-table">
      <thead>
        <tr>
          <th className="downloads-table__header-cell downloads-table__cell--checkbox"></th>
          <th className="downloads-table__header-cell">Name</th>
          <th className="downloads-table__header-cell">Device</th>
          <th className="downloads-table__header-cell">Path</th>
          <th className="downloads-table__header-cell downloads-table__header-cell-status">Status</th>
        </tr>
      </thead>
      <tbody>
        {items.map(item => (
          <DownloadsTableRow key={item.name} item={item} onToggleSelection={onToggleSelection} />
        ))}
      </tbody>
    </table>
  );
}

DownloadsTable.propTypes = {
  items: PropTypes.array.isRequired,
  onToggleSelection: PropTypes.func.isRequired
};

export default DownloadsTable;
