import React, { useState, useEffect } from 'react';

import { getDownloads } from 'services/downloads';
import Downloads from './components/Downloads';

function App() {
  // Note: I assume Downloads component to be stateless and it receives the data from container/service/controller
  const [downloads, setDownloads] = useState(undefined);

  useEffect(() => {
    fetchData();
  }, []);

  async function fetchData() {
    try {
      const response = await getDownloads();

      setDownloads(response);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <>
      {downloads && <Downloads items={downloads} />}
    </>
  );
}

export default App;
