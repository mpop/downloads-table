function isDownloadAvailable(status) {
  return status === 'available';
}

export default isDownloadAvailable;
